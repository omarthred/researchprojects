package im.thred.kudanartest;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import eu.kudan.kudan.ARAPIKey;
import eu.kudan.kudan.ARActivity;
import eu.kudan.kudan.ARImageNode;
import eu.kudan.kudan.ARImageTrackable;
import eu.kudan.kudan.ARImageTrackableListener;
import eu.kudan.kudan.ARImageTracker;

public class MainActivity extends ARActivity implements ARImageTrackableListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//
//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
        ARAPIKey key = ARAPIKey.getInstance();
        key.setAPIKey("tADEjbPwoTWA6A4Dfa3wHdqNmxN8foY/HgId1/9nbkivdV5KjtH3Aq0/tLDlDI7Kacw5vfuuC3MDYjKTXGGhyD8und2ef8a3FnjxpCbQJmMHMn6YfuknKn/udFtASir7eFk4O7HM+LJ5O+t9P6D4fDWv0phQ+kAEf1Cs+7aZZlf0WTSdiqOPaAZdtFMVbANmXC77suw9GCl0V8rQWxzaCmwYL+oXd6qQeR6DDKvFWt9jNCRl48MFQsXxTy0Qx0DrrUeLo8DomJAqfGujNtIRm8uZHJBgz1sHtmwc3qjXa8YoJmZhRZNLTbP4SC+PLMmYjoB/iGHAf1aquT/VcrIO6jcRo88O+XR7bPQCmN66I/ZFfcsF5ZPeTSGeHlipCkYH986MBRGFsjpvV1pnruITxwVpshKAbh3/mlVH23RAnCw9T5NAPHVl+QC6cM9gaJ7V7jkA4Cclzd3GQYTdjoW8c83LAgCfeaI3u0mnWqrEvJzUP0Dyb81Zh7UqllenGSyKd9652BFi893U3cq0cEp2laIFhF73kDmyKz5ob2LNWOjnF3SQyGZB53bZVPyczF3yPabh6Sz50YElCyoQsk8HOtUj1nYQ0UW0TC2dlHaFT6dzCJNLSNbp3wO4XCFotOYgZ7XUngFjyWsgOBrykfgK+h13Dp3ocq6SHUVG8QiUQSI=");
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

    @Override
    public void setup()
    {
        super.setup();

//        // Initialise the image trackable and load the image.
//        ARImageTrackable imageTrackable = new ARImageTrackable("QR Marker");
//        imageTrackable.loadFromAsset("file:///android_asset/qr_code_2.jpg");
//
//        // Get the single instance of the image tracker.
//        ARImageTracker imageTracker = ARImageTracker.getInstance();
//
//        //Add the image trackable to the image tracker.
//        imageTracker.addTrackable(imageTrackable);
//
//        // Initialise the image node with our image
//        ARImageNode imageNode = new ARImageNode("file:///android_asset/KudanCow.png");
//
//// Add the image node as a child of the trackable's world
//        imageTrackable.getWorld().addChild(imageNode);
//
//        // Add listener methods that are defined in the ARImageTrackableListener interface
//        imageTrackable.addListener(this);

        //---------------

        // Create our trackable with an image
        ARImageTrackable trackable = createTrackable("Marker","qr_code_2.jpg");

        // Get the trackable Manager singleton
        ARImageTracker trackableManager = ARImageTracker.getInstance();
        trackableManager.initialise();
        //Add image trackable to the image tracker manager
        trackableManager.addTrackable(trackable);

        // Create an image node using an image of the kudan cow
        ARImageNode cow = new ARImageNode(String.format("KudanCow.png"));

        // Add the image node as a child of the trackable's world
        trackable.getWorld().addChild(cow);

        // Add listener methods that are defined in the ARImageTrackableListener interface
        trackable.addListener(this);
    }

    private ARImageTrackable createTrackable(String trackableName, String assetName)
    {
        // Create a new trackable instance with a name
        ARImageTrackable trackable = new ARImageTrackable(trackableName);

        // Load the  image for this marker
        trackable.loadFromAsset(String.format(assetName));

        return trackable;
    }

    @Override
    public void didDetect(ARImageTrackable arImageTrackable) {
        Log.d("Marker","Did Detect");
    }

    @Override
    public void didTrack(ARImageTrackable arImageTrackable) {
        Log.d("Marker","Did Track");
    }

    @Override
    public void didLose(ARImageTrackable arImageTrackable) {
        Log.d("Marker","Did Lose");
    }
}
