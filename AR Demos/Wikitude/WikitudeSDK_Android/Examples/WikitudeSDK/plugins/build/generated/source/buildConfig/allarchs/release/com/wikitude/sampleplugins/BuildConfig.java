/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.wikitude.sampleplugins;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.wikitude.sampleplugins";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "allarchs";
  public static final int VERSION_CODE = -1;
  public static final String VERSION_NAME = "";
}
