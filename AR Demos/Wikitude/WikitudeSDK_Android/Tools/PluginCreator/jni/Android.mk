LOCAL_PATH := $(call my-dir)/..
SRC_DIR := $(LOCAL_PATH)/src
LIB_DIR := $(LOCAL_PATH)/lib
INCLUDE_DIR := $(LOCAL_PATH)/include

include $(CLEAR_VARS)

LOCAL_PATH := $(LIB_DIR)/$(TARGET_ARCH_ABI)
include $(CLEAR_VARS)
LOCAL_MODULE    := WikitudePlugins
LOCAL_SRC_FILES := libWikitudePlugins.a
include $(PREBUILT_STATIC_LIBRARY)

LOCAL_PATH := $(SRC_DIR)
include $(CLEAR_VARS)

LOCAL_MODULE := samplePlugin

LOCAL_C_INCLUDES := $(INCLUDE_DIR)/wikitude \
    $(SRC_DIR)/__your_plugin__ \

LOCAL_SRC_FILES := jniHelper.cpp JniRegistration.cpp __your_plugin__/__YOUR_PLUGIN__.cpp

LOCAL_STATIC_LIBRARIES += WikitudePlugins
LOCAL_LDLIBS += -llog

include $(BUILD_SHARED_LIBRARY)
