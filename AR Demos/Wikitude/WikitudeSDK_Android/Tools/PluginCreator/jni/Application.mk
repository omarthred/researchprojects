APP_MODULES := samplePlugin
APP_PLATFORM := android-15
APP_STL := c++_static
NDK_TOOLCHAIN_VERSION := clang

APP_CPPFLAGS := \
	-frtti \
	-fexceptions 
	-std=c++14

APP_ABI := armeabi-v7a arm64-v8a x86
APP_OPTIM := debug
NDK_DEBUG := 1
